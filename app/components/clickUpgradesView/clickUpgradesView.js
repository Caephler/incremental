(function() {
    var app = angular.module('incrementalApp');
    
    var buyUpgrade = function (key, ClickUpgradesHelper, CurrencyObject) {
        var upgrade = ClickUpgradesHelper.getUpgrade(key);
        if (CurrencyObject.currency >= upgrade[key].cost()) {
            CurrencyObject.currency -= upgrade[key].cost();
            CurrencyObject.cpc += upgrade[key].cpc;
            upgrade[key].numOwned++;
            ClickUpgradesHelper.setUpgrades(upgrade);
            
            return true
        } else {
            return false
        }
    }
    
    var clickUpgradesController = function(ClickUpgradesHelper, CurrencyObject, ConsoleEventHandler) {
        var ctrl = this;
        this.availUpgrades = ClickUpgradesHelper.clickUpgrades;
        
        this.buyUpgrade = function(key) {
            var upgrade = ClickUpgradesHelper.getUpgrade(key)[key];
            var name = upgrade.name;
            var cost = upgrade.cost();
            if(buyUpgrade(key, ClickUpgradesHelper, CurrencyObject)) {
                ConsoleEventHandler.pushMessage('Bought an upgrade (' + name + ') for $m ' + cost);
            } else {
                ConsoleEventHandler.pushMessage('Unable to buy upgrade (' + name + ') due to insufficient $m !');
            }
        };
        
        this.buyMaxUpgrade = function(key) {
            var upgrade = ClickUpgradesHelper.getUpgrade(key)[key];
            var cost = 0;
            var count = 0;
            while(CurrencyObject.currency >= upgrade.cost()) {
                cost += upgrade.cost();
                count++;
                buyUpgrade(key, ClickUpgradesHelper, CurrencyObject);
            }
            if(count > 0) {
                ConsoleEventHandler.pushMessage('Bought <strong>' + count + '</strong> upgrades (' + upgrade.name + ') for $m ' + cost);
            } else {
                ConsoleEventHandler.pushMessage('Unable to buy upgrade (' + upgrade.name + ') due to insufficient $m !');
            }
        }
    }
    
    app.component('clickUpgradesView', {
        controller: clickUpgradesController,
        templateUrl: 'app/components/clickUpgradesView/click-upgrades-view.html'
    });
})();