(function() {
    var app = angular.module('incrementalApp');
    
    var consoleController = function(ConsoleEventHandler, $sce) {
        var ctrl = this;
        this.messages = ConsoleEventHandler.messages;
        this.missions = ConsoleEventHandler.missions;
        
        this.clearMessages = function() {
            ConsoleEventHandler.clear();
        };
        
        this.sanitize = function(htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    };
    
    app.component('console', {
        controller: consoleController,
        templateUrl: 'app/components/console/console.html'
    });
})();