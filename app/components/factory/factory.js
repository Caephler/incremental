(function() {
    var app = angular.module('incrementalApp');
    
    var buyFactory = function(key, FactoryHelper, CurrencyObject) {
        var factory = FactoryHelper.getFactory(key);
        if(CurrencyObject.currency >= factory[key].cost()) {
            CurrencyObject.currency -= factory[key].cost();
            factory[key].numOwned++;
            FactoryHelper.setFactories(factory);
            
            return true
        } else {
            return false
        }
    }
    
    var factoryController = function(FactoryHelper, CurrencyObject, ConsoleEventHandler) {
        var ctrl = this;
        this.availUpgrades = FactoryHelper.factories;
        
        this.buyFactory = function(key) {
            var factory = FactoryHelper.getFactory(key)[key];
            var name = factory.name;
            var cost = factory.cost();
            if(buyFactory(key, FactoryHelper, CurrencyObject)) {
                ConsoleEventHandler.pushMessage('Bought a factory (' + name + ') for $m ' + cost);
            } else {
                ConsoleEventHandler.pushMessage('Unable to buy factory (' + name + ') due to insufficient $m !');
            }
        }
        this.buyMaxFactories = function(key) {
            var factory = FactoryHelper.getFactory(key)[key];
            var cost = 0;
            var count = 0;
            while(CurrencyObject.currency >= factory.cost()) {
                cost += factory.cost();
                count++;
                buyFactory(key, FactoryHelper, CurrencyObject);
            }
            if(count > 0) {
                ConsoleEventHandler.pushMessage('Bought <strong>' + count + '</strong> factories (' + factory.name + ') for $m ' + cost);
            } else {
                ConsoleEventHandler.pushMessage('Unable to buy upgrade (' + factory.name + ') due to insufficient $m !');
            }
        }
    };
    
    app.component('factory', {
        controller: factoryController,
        templateUrl: 'app/components/factory/factory.html'
    });
})();