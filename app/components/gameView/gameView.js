(function () {
    var app = angular.module('incrementalApp');

    function mainButtonOnClick(CurrencyObject, $event, $timeout) {
        CurrencyObject.currency += CurrencyObject.cpc;
        var x = $event.clientX;
        var y = $event.clientY;
        var animation = angular.element('<div class="unselectable popup animation" style="left:' + x + ';top:' + y + '">+' + CurrencyObject.cpc +'</div>');
        var clickingPane = angular.element($event.target);
        clickingPane.append(animation);
        $timeout(function () {
            animation.remove();
        }, 450);
    };

    var gameController = function ($interval, $timeout, TimeHelper, SaveData, UpgradesHelper, FactoryHelper, CurrencyObject, InitHelper, ConsoleEventHandler, Story, StatsTracker, ClickUpgradesHelper, Utilities) {
        var ctrl = this;
        //Variable Initialization
        this.CurrencyObject = CurrencyObject;
        this.navState = "upgrades";
        this.manufacturingCycle = 60000;
        //Method Initialization
        InitHelper.initialize();
        //Interactive Methods
        this.mainButtonOnClick = function ($event) {
            console.log($event);
            mainButtonOnClick(CurrencyObject, $event, $timeout);
            StatsTracker.stats.totalEarned += CurrencyObject.cpc;
            StatsTracker.stats.totalClicks++;
        }
        this.save = function () {
                SaveData.setSaveData(CurrencyObject);
                SaveData.setSaveData(StatsTracker);
                SaveData.setSaveData(Story.saveObj());
                var saveTime = TimeHelper.getCurrentRawTime();
                SaveData.setSaveData({
                    'lastSavedTime': saveTime
                });
                UpgradesHelper.saveUpgrades();
                ClickUpgradesHelper.saveUpgrades();
                FactoryHelper.saveFactories();
                ConsoleEventHandler.pushMessage('Data saved!');
            }
            //Refresh Loop
        $interval(function () {
            CurrencyObject.currency += CurrencyObject.cps;
            StatsTracker.stats.totalEarned += CurrencyObject.cps;
            Story.checkConditions();
        }, 1000);
        $interval(function() {
            var upgrades = {};
            var cpsIncrease = 0;
            var str = '';
            for(factory in FactoryHelper.factories) {
                var factory = FactoryHelper.getFactory(factory)[factory];
                if(factory.numOwned > 0) {
                    var upgrade = UpgradesHelper.getUpgrade(factory.upgradeKey)[factory.upgradeKey];
                    UpgradesHelper.increaseUpgrade(factory.upgradeKey, factory.numOwned * factory.upgradeCount);
                    CurrencyObject.cps += upgrade.cps * factory.numOwned * factory.upgradeCount;
                    if(!upgrades[factory.upgradeKey]) {
                        upgrades[factory.upgradeKey] = {'name': upgrade.name,
                                                        'upgradeCount': 0};
                    }
                    upgrades[factory.upgradeKey].upgradeCount += factory.numOwned * factory.upgradeCount;
                    cpsIncrease += upgrade.cps * factory.numOwned * factory.upgradeCount;
                }
            }
            if(Utilities.isNotEmpty(upgrades)) {
                str = 'Manufacturing Cycle: ';
                for(upgrade in upgrades) {
                    str += upgrades[upgrade].upgradeCount + ' ' + upgrades[upgrade].name + '(s), ';
                }
                str = str.substring(0, str.length - 2);
                ConsoleEventHandler.pushMessage(str);
            }
        }, ctrl.manufacturingCycle)
        $interval(function () {
            ctrl.save();
        }, 300000);
    };

    app.component('gameView', {
        controller: gameController,
        templateUrl: 'app/components/gameView/game-view.html'
    });
})();
