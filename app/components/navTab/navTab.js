(function() {
    var app = angular.module('incrementalApp');
    
    function NavButton(state, icon) {
        this.state = state;
        this.icon = "fa fa-" + icon + ' fa-2x';
    }
    var navTabController = function() {
        var ctrl = this;
        this.navButtons = [
            new NavButton('upgrades', 'wrench'),
            new NavButton('factory', 'asterisk'),
            new NavButton('click-upgrades', 'hand-pointer-o'),
            new NavButton('stats', 'bar-chart'),
        ];
        this.navChange = function(state) {
            ctrl.navState = state;
        };
    };
    
    app.component('navTab', {
        bindings: {
            navState: '='
        },
        controller: navTabController,
        templateUrl: 'app/components/navTab/nav-tab.html'
    });
})();