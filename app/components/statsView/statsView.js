(function() {
    var app = angular.module('incrementalApp');
    
    var statsController = function(StatsTracker, SaveData) {
        this.stats = StatsTracker.stats;
        this.saveStats = SaveData.getSaveData();
    };
    
    app.component('statsView', {
        controller: statsController,
        templateUrl: 'app/components/statsView/stats-view.html'
    })
})();