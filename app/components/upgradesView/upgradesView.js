(function () {
    var app = angular.module('incrementalApp');

    var buyUpgrade = function (key, UpgradesHelper, CurrencyObject) {
        var upgrade = UpgradesHelper.getUpgrade(key);
        if (CurrencyObject.currency >= upgrade[key].cost()) {
            CurrencyObject.currency -= upgrade[key].cost();
            CurrencyObject.cps += upgrade[key].cps;
            upgrade[key].numOwned++;
            UpgradesHelper.setUpgrades(upgrade);
            
            return true
        } else {
            return false
        }
    }

    var upgradeController = function (UpgradesHelper, CurrencyObject, ConsoleEventHandler) {
        var ctrl = this;
        this.availUpgrades = UpgradesHelper.upgrades;

        this.buyUpgrade = function (upgradeKey) {
            var upgrade = UpgradesHelper.getUpgrade(upgradeKey)[upgradeKey];
            var name = upgrade.name;
            var cost = upgrade.cost();
            if(buyUpgrade(upgradeKey, UpgradesHelper, CurrencyObject)) {
                ConsoleEventHandler.pushMessage('Bought an upgrade (' + name + ') for $m ' + cost);
            } else {
                ConsoleEventHandler.pushMessage('Unable to buy upgrade (' + name + ') due to insufficient $m !');
            }
        };
        this.buyMaxUpgrade = function(key) {
            var upgrade = UpgradesHelper.getUpgrade(key)[key];
            var cost = 0;
            var count = 0;
            while(CurrencyObject.currency >= upgrade.cost()) {
                cost += upgrade.cost();
                count++;
                buyUpgrade(key, UpgradesHelper, CurrencyObject);
            }
            if(count > 0) {
                ConsoleEventHandler.pushMessage('Bought <strong>' + count + '</strong> upgrades (' + upgrade.name + ') for $m ' + cost);
            } else {
                ConsoleEventHandler.pushMessage('Unable to buy upgrade (' + upgrade.name + ') due to insufficient $m !');
            }
        }
    };

    app.component('upgradesView', {
        templateUrl: 'app/components/upgradesView/upgrades-view.html',
        controller: upgradeController
    });
})();
