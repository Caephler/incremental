(function() {
    var app = angular.module('incrementalApp');
    
    app.directive('backgroundImg', function() {
        return function(scope, element, attrs) {
            element.css({
                'background-image': 'url(' + attrs.backgroundImg + ')',
                'background-size': 'contain',
                'background-repeat': 'no-repeat',
                'background-position': 'center center'
            });
        }
    });
})();