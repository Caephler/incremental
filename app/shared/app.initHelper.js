(function () {
    var app = angular.module('incrementalApp');
    
    function Upgrade(name, baseCost, cps, imgPath) {
        this.name = name;
        this.baseCost = baseCost;
        this.cps = cps;
        this.imgPath = imgPath;
        this.numOwned = 0;
        
        this.getImgPath = function() {
            return 'assets/img/pic/' + this.imgPath
        };
        this.cost = function() {
            var cost = Math.max(Math.floor(this.baseCost * Math.pow(this.numOwned, 1.15)), this.baseCost);
            return cost
        }
    }
    
    function ClickUpgrade(name, baseCost, cpc, imgPath) {
        this.name = name;
        this.baseCost = baseCost;
        this.cpc = cpc;
        this.imgPath = imgPath;
        this.numOwned = 0;
        
        this.getImgPath = function() {
            return 'assets/img/pic/' + this.imgPath
        };
        this.cost = function() {
            var cost = Math.max(Math.floor(this.baseCost * Math.pow(this.numOwned, 1.15)), this.baseCost);
            return cost
        }
    }
    
    function Factory(name, baseCost, upgradeKey, upgradeCount, imgPath, UpgradesHelper) {
        this.name = name;
        this.baseCost = baseCost;
        this.upgradeKey = upgradeKey;
        this.upgradeCount = upgradeCount;
        this.imgPath = imgPath;
        this.numOwned = 0;
        
        this.getUpgradeName = function () {
            return UpgradesHelper.getUpgrade(upgradeKey)[upgradeKey].name;
        }
        this.getImgPath = function() {
            return 'assets/img/pic/' + this.imgPath
        };
        this.cost = function() {
            var cost = Math.max(Math.floor(this.baseCost * Math.pow(this.numOwned, 2.5)), this.baseCost);
            return cost
        }
    }

    app.factory('InitHelper', function (TimeHelper, ClickUpgradesHelper, UpgradesHelper, FactoryHelper, CurrencyObject, SaveData, Story, Utilities, ConsoleEventHandler, StatsTracker) {
        var save = SaveData.getSaveData();
        const UPGRADE_LIST = {
            'Upgrade_1': new Upgrade('Small Chip', 10, 1, 'smallchip.png'),
            'Upgrade_2': new Upgrade('Medium Chip', 50, 3, 'medchip.png'),
            'Upgrade_3': new Upgrade('Big Chip', 100, 5, 'bigchip.png'),
            'Cheat': new Upgrade('Cheat', 10, 1000, 'cheat.png')
        };
        const CLICK_UPGRADE_LIST = {
            'Pointer': new ClickUpgrade('Mighty Pointer', 5, 1, 'clenched-fist.svg'),
            'Target': new ClickUpgrade('Targeted Clicks', 50, 8, 'weapon-crosshair-2.svg')
        };
        const FACTORY_LIST = {
            'Sweatshop': new Factory('Chip Sweatshop', 1000, 'Upgrade_1', 1, 'old_computer.png', UpgradesHelper),
            'Factory': new Factory('Chip Factory', 2000, 'Upgrade_1', 3, 'controller.png', UpgradesHelper)
        };
        return {
            initialize: function () {
                
                var upgrades = UPGRADE_LIST;
                if (Utilities.isNotEmpty(save.upgrades)) {
                    for (key in upgrades) {
                        if (!save.upgrades[key]) {
                            save.upgrades[key] = {
                                'numOwned': 0
                            };
                        }
                        upgrades[key].numOwned = save.upgrades[key].numOwned;
                    }

                } else {
                    for (key in upgrades) {
                        upgrades[key].numOwned = 0;
                    }
                }
                UpgradesHelper.setUpgrades(upgrades);
                
                var clickUpgrades = CLICK_UPGRADE_LIST;
                if (Utilities.isNotEmpty(save.clickUpgrades)) {
                    for (key in clickUpgrades) {
                        if (!save.clickUpgrades[key]) {
                            save.clickUpgrades[key] = {
                                'numOwned': 0
                            };
                        }
                        
                        clickUpgrades[key].numOwned = save.clickUpgrades[key].numOwned;
                    }
                } else {
                    for (key in clickUpgrades) {
                        clickUpgrades[key].numOwned = 0;
                    }
                }
                ClickUpgradesHelper.setUpgrades(clickUpgrades);
                
                var factories = FACTORY_LIST;
                if (Utilities.isNotEmpty(save.factories)) {
                    for (key in factories) {
                        if (!save.factories[key]) {
                            save.factories[key] = {
                                'numOwned': 0
                            };
                        }
                        factories[key].numOwned = save.factories[key].numOwned;
                    }

                } else {
                    for (key in factories) {
                        factories[key].numOwned = 0;
                    }
                }
                FactoryHelper.setFactories(factories);

                CurrencyObject.currency = (save.currency) ? save.currency : 0;
                StatsTracker.stats = (save.stats) ? save.stats : {};
                CurrencyObject.cps = 0;
                CurrencyObject.cpc = 1;
                
                for (key in upgrades) {
                    CurrencyObject.cps += upgrades[key].numOwned * upgrades[key].cps;
                }
                for (key in clickUpgrades) {
                    CurrencyObject.cpc += clickUpgrades[key].numOwned * clickUpgrades[key].cpc;
                }

                var storyboardEvents = Story.storyboardEvents;
                for (event in save.storyboardEvents) {
                    storyboardEvents[event].activated = save.storyboardEvents[event].activated;
                }
                Story.setEvents(storyboardEvents);

                if (save.lastSavedTime) {
                    var currentTime = TimeHelper.getCurrentRawTime();
                    var lastSavedTime = save.lastSavedTime;
                    var secDiff = Math.floor((currentTime - lastSavedTime) / 1000);
                    var cps = save.cps | 0;
                    var earnings = Math.floor(cps * secDiff * (25 / 100));
                    if (secDiff > 0) {
                        ConsoleEventHandler.pushMessage("You'ved earned " + earnings + " $m since you were here! (25% efficiency)");
                        CurrencyObject.currency += earnings;
                        StatsTracker.stats.totalEarned += earnings;
                    }
                }
            }
        }
    });
})();
