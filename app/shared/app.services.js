(function () {
    var app = angular.module('incrementalApp');

    app.factory('ConsoleEventHandler', function (TimeHelper, $filter, Utilities) {
        var messages = {
            'messageLog': []
        };
        var missions = {
            'currentMission': {},
            'missionLog': [],
            'active': false
        };

        return {
            messages: messages,
            missions: missions,
            pushMessage: function (msg) {
                var msgObj = {
                    'timeStamp': TimeHelper.convertTime(Date.now()),
                    'message': $filter('replaceCurrency')(msg)
                };
                this.messages.messageLog.push(msgObj);
                if (this.messages.messageLog.length > 30) {
                    this.messages.messageLog.shift();
                }
            },
            pushMission: function (header, message, objective) {
                var htmlMessage = "<div class='mission'><div class='header'>" + header + "</div><div class='objective'>" + $filter('replaceCurrency')(objective) + "</div><span>" + $filter('replaceCurrency')(message) + "</span></div>";
                var msgObj = {
                    'message': htmlMessage
                };
                this.messages.messageLog.push(msgObj);
                if (this.messages.messageLog.length > 30) {
                    this.messages.messageLog.shift();
                }
                var currentMission = {
                    'name': header,
                    'objective': objective
                };
                if (Utilities.isEmpty(this.missions.currentMission)) {
                    this.missions.missionLog.push(this.missions.currentMission);
                    this.missions.active = true;
                }
                this.missions.currentMission = currentMission;
            },
            clear: function () {
                this.messages.messageLog = [];
            }
        }
    });

    app.factory('TimeHelper', function ($filter) {
        var dateFormat = 'dd/MM/yyyy HH:mm:ss';
        var timeFormat = 'HH:mm:ss';

        return {
            getCurrentTime: function () {
                var currentTime = Date.now();
                currentTime = $filter('date')(currentTime, dateFormat);
                return currentTime;
            },
            getCurrentRawTime: function () {
                var currentTime = Date.now();
                return currentTime
            },
            convertTime: function (time) {
                return $filter('date')(time, timeFormat);
            }
        }
    });

    app.factory('SaveData', function (TimeHelper) {
        var saveData;
        if (!localStorage.getItem('gameSaveData')) {
            var startTime = TimeHelper.getCurrentRawTime();
            saveData = {
                'currency': 0,
                'cps': 0,
                'upgrades': {},
                'clickUpgrades': {},
                'factories': {},
                'stats': {
                    'startTime': startTime,
                    'totalEarned': 0,
                    'totalClicks': 0
                }
            };
        } else {
            saveData = JSON.parse(localStorage.getItem('gameSaveData'));
        }

        return {
            getSaveData: function () {
                return saveData
            },
            setSaveData: function (obj) { //Accepts Object
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        saveData[prop] = obj[prop];
                    }
                }
                var jsonSaveStr = JSON.stringify(saveData);
                localStorage.setItem('gameSaveData', jsonSaveStr);
            }
        }
    });
    
    app.factory('ClickUpgradesHelper', function(SaveData) {
        var clickUpgrades = {};
        
        return {
            clickUpgrades: clickUpgrades,
            getUpgrade: function(key) {
                var obj = {};
                obj[key] = this.clickUpgrades[key];
                return obj;
            },
            setUpgrades: function(upgradeObj) {
                for (key in upgradeObj) {
                    this.clickUpgrades[key] = upgradeObj[key];
                };
            },
            increaseUpgrade: function(key, num) {
                if(!num) { num = 1; }
                this.clickUpgrades[key].numOwned += num;
            },
            saveUpgrades: function() {
                var saveObj = {
                    'clickUpgrades': {}
                };
                for (key in this.clickUpgrades) {
                    saveObj.clickUpgrades[key] = {};
                    saveObj.clickUpgrades[key].numOwned = this.clickUpgrades[key].numOwned;
                }
                SaveData.setSaveData(saveObj);
            }
        }
    });
    
    app.factory('UpgradesHelper', function (SaveData) {
        var upgrades = {};

        return {
            upgrades: upgrades,
            getUpgrade: function (key) {
                var obj = {};
                obj[key] = this.upgrades[key];
                return obj;
            },
            setUpgrades: function (upgradeObj) {
                for (key in upgradeObj) {
                    this.upgrades[key] = upgradeObj[key];
                };
            },
            increaseUpgrade: function (key, num) {
                if(!num) { num = 1; }
                this.upgrades[key].numOwned += num;
            },
            saveUpgrades: function () {
                var saveObj = {
                    'upgrades': {}
                };
                for (key in this.upgrades) {
                    saveObj.upgrades[key] = {};
                    saveObj.upgrades[key].numOwned = this.upgrades[key].numOwned;
                }
                SaveData.setSaveData(saveObj);
            }
        }
    });
    
    app.factory('FactoryHelper', function(SaveData) {
        var factories = {};
        
        return {
            factories: factories,
            getFactory: function(key) {
                var obj = {};
                obj[key] = this.factories[key];
                return obj;
            },
            setFactories: function(factoryObj) {
                for (key in factoryObj) {
                    this.factories[key] = factoryObj[key];
                };
            },
            increaseFactory: function(key, num) {
                if(!num) { num = 1; }
                this.factories[key].numOwned += num;
            },
            saveFactories: function () {
                var saveObj = {
                    'factories': {}
                };
                for (key in this.factories) {
                    saveObj.factories[key] = {};
                    saveObj.factories[key].numOwned = this.factories[key].numOwned;
                }
                SaveData.setSaveData(saveObj);
            }
        }
    });
    
    app.factory('StatsTracker', function() {
        var StatsTracker = {'stats': {}};
        return StatsTracker;
    });

    app.factory('CurrencyObject', function () {
        var CurrencyObject = {};
        return CurrencyObject
    });

    
})();
