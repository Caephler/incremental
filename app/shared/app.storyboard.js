(function () {
    var app = angular.module('incrementalApp');

    app.factory('Story', function (CurrencyObject, ConsoleEventHandler, UpgradesHelper) {
        var storyboardEvents = {
            'Tutorial': {
                'conditions': {
                    'one': function () {
                        return true
                    }
                },
                'event': {
                    'one': function () {
                        ConsoleEventHandler.pushMission("Tutorial","Click the button to gain $m to buy upgrades! Try earning at least 10 $m !", "Earn 10 $m");
                    }
                },
                'activated': false
            },
            'Tutorial 2': {
                'conditions': {
                    'one': function() {
                        return CurrencyObject.currency >= 10
                    }
                },
                'event': {
                    'one': function() {
                        ConsoleEventHandler.pushMission("Tutorial 2","Yay! You've earnd enough $m to buy an upgrade! Buy the first upgrade!", "Buy Upgrade 1");
                    }
                },
                'activated': false
            },
            'Tutorial 3': {
                'conditions': {
                    'one': function() {
                        return UpgradesHelper.getUpgrade('Upgrade_1').Upgrade_1.numOwned > 0
                    }
                },
                'event': {
                    'one': function() {
                        ConsoleEventHandler.pushMessage("Yay you've completed the tutorial! Now watch the $m roll in!");
                    },
                    'two': function() {
                        ConsoleEventHandler.pushMission("First Steps", "You've gotta earn some capital first. Earn 1000 $m !", "Earn 1000 $m");
                    }
                }
            },
            'First Steps': {
                'conditions': {
                    'one': function() {
                        return CurrencyObject.currency >= 1000
                    }
                },
                'event': {
                    'one': function() {
                        ConsoleEventHandler.pushMessage("You've got the $m . Now, use it to buy some juicy upgrades!");
                    },
                    'two': function() {
                        ConsoleEventHandler.pushMission("Upgrades are the Key!", "Use your hard earned $m to buy some upgrades! Let your upgrades work for you.", "Have a total of at least 10 upgrades.");
                    }
                }
            }
        };
        return {
            storyboardEvents: storyboardEvents,
            checkConditions: function () {
                for (event in this.storyboardEvents) {
                    var ev = this.storyboardEvents[event];
                    if (!ev.activated) {
                        var state = true;
                        for (condition in ev.conditions) {
                            if (!ev.conditions[condition]()) {
                                state = false;
                            }
                        }
                        if (state) {
                            for (result in ev.event) {
                                ev.event[result]();
                            }
                            ev.activated = true;
                        }
                    }
                }
            },
            setEvents: function(obj) {
                this.storyboardEvents = obj;
            },
            saveObj: function() {
                var saveObj = {};
                for(event in this.storyboardEvents) {
                    var ev = this.storyboardEvents[event];
                    saveObj[event] = {};
                    saveObj[event].activated = ev.activated;
                }
                return {'storyboardEvents': saveObj}
            }
        }
    })
})();
