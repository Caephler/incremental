(function () {
    var app = angular.module('incrementalApp');

    app.factory('Utilities', function () {
        return {
            isEmpty: function (obj) {
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        return false
                    }
                }

                return true && JSON.stringify(obj) === JSON.stringify({});
            },
            isNotEmpty: function (obj) {
                return !this.isEmpty(obj);
            }
        }
    });

    app.filter('replaceCurrency', function () {
        return function (str) {
            var strArray = str.split(' ');
            var replacedStr = strArray.reduce(function (str, curr) {
                curr = (curr === '$m') ? '<i class="fa fa-money"></i>' : curr;
                return str + ' ' + curr
            });
            return replacedStr
        }

    });
})();
